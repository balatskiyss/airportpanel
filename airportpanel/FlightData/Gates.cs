﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportPanel.FlightData
{
    enum Terminals { A = 1, B, D, F };

    class Gates
    {
        public static string[] getGatesArray()
        {

            string[] gates = new string[20];
            int j = 1;

            for (int i = 0; i < gates.Length; i += 4)
            {
                gates[i] = Terminals.A.ToString() + j;
                gates[i + 1] = Terminals.B.ToString() + j;
                gates[i + 2] = Terminals.D.ToString() + j;
                gates[i + 3] = Terminals.F.ToString() + j;
                j++;
            }
            return gates;
        }
    }
}
