﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportPanel.FlightData
{
    class DestinationCityAndPort
    {
        public static string[] getCityArray() {
            var path = @"C:\Users\Use\source\repos\AirportPanel\AirportPanel\FlightDataRepository\CItyAndPorts.txt";
            string[] cities = new string[20];

            try {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default)) {
                    string line;
                    int i = 0;
                    while (((line = sr.ReadLine()) != null)) {
                        i++;
                        cities[i] = line;
                    }
                }
            }
            catch (Exception e) {
               // Console.WriteLine(e.Message);
            }
            return cities;
        }
    }
}
