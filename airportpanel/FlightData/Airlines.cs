﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportPanel.FlightData
{
    class Airlines
    {
        public static string[] getAirlinesArray()
        {
            var path = @"C:\Users\Use\source\repos\AirportPanel\AirportPanel\FlightDataRepository\Airlines.txt";
            string[] airlines = new string[7];

            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    int i = 0;
                    while (((line = sr.ReadLine()) != null))
                    {
                        airlines[i] = line;
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return airlines;
        }

        
    }
}
