﻿using AirportPanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportPanel
{
    public enum FlightDirection { departure, arrival }
    public enum FlightStatus { unknown = 1, arrived, checkIn, gateClosed, delayed, canceled, inFlight }

    public struct Flight
    {
        public string airline;
        public string flightNumber;
        public string destinationCityAndPort;
        public string terminal;
        public FlightStatus flightStatus;
        public string gate;
        public FlightDirection flightDirection;
        public DateTime departureDateTime;
        public DateTime arriveDateTime;
        public string flightTime;


        public string getAirline()
        {
            int arrayLength = FlightData.Airlines.getAirlinesArray().Length;
            int random = new Random().Next(0, arrayLength);

            string airline = FlightData.Airlines.getAirlinesArray()[random];

            return airline;
        }

        public string getFlightNumber(string airline)
        {
            string abbriviationPart;
           // System.Threading.Thread.Sleep(500);
            int midIndex = airline.Length / 2;

            if (airline.Contains(" "))
            {
                int space = airline.IndexOf(" ");
                airline = airline.Replace(" ", "");
                abbriviationPart = (airline.Substring(0, 1) + airline.Substring(space, 1)).ToUpper();
            }
            else
            {
                abbriviationPart = (airline.Substring(0, 1) + airline.Substring(midIndex, 1)).ToUpper();
            }

            int utcNow = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            string numberPart = utcNow.ToString();
            numberPart = numberPart.Substring(numberPart.Length - 4, 4);

            return flightNumber = abbriviationPart + " " + numberPart;
        }

        public string getCity()
        {
            int arrayLength = FlightData.DestinationCityAndPort.getCityArray().Length;
            int random = new Random().Next(0, arrayLength);

            string city = FlightData.DestinationCityAndPort.getCityArray()[random];

            return city;
        }

        public string getGate()
        {
            int arrayLength = FlightData.Gates.getGatesArray().Length;
            int random = new Random().Next(0, arrayLength);

            string gate = FlightData.Gates.getGatesArray()[random];

            return gate;
        }

        public string getTerminal()
        {
            string terminal = getGate().Substring(0, 1);

            return terminal;
        }

        public FlightDirection getFlightDirection()
        {
            Array values = Enum.GetValues(typeof(FlightDirection));
            Random random = new Random();
            FlightDirection flightDirection = (FlightDirection)values.GetValue(random.Next(values.Length));

            return flightDirection;
        }

        public FlightStatus getFlightStatus()
        {
            FlightStatus flighStatus;
            Random random = new Random();

            FlightStatus[] departuredFlightStatuses = new FlightStatus[5];
            departuredFlightStatuses[0] = FlightStatus.unknown;
            departuredFlightStatuses[1] = FlightStatus.checkIn;
            departuredFlightStatuses[2] = FlightStatus.gateClosed;
            departuredFlightStatuses[3] = FlightStatus.delayed;
            departuredFlightStatuses[4] = FlightStatus.canceled;

            FlightStatus[] arrivedFlightStatuses = new FlightStatus[5];
            arrivedFlightStatuses[0] = FlightStatus.unknown;
            arrivedFlightStatuses[1] = FlightStatus.arrived;
            arrivedFlightStatuses[2] = FlightStatus.inFlight;
            arrivedFlightStatuses[3] = FlightStatus.delayed;
            arrivedFlightStatuses[4] = FlightStatus.canceled;

            if (getFlightDirection() == FlightDirection.departure)
            {
                flightStatus = (FlightStatus)departuredFlightStatuses.GetValue(random.Next(departuredFlightStatuses.Length));
            }
            else if (getFlightDirection() == FlightDirection.arrival)
            {
                flightStatus = (FlightStatus)arrivedFlightStatuses.GetValue(random.Next(arrivedFlightStatuses.Length));
            }
            else
            {
                Console.WriteLine("Error. Incorrect flight status.");
            }
            return flightStatus;
        }

        public string getUnknownDateTime()
        {
            string dateTime = null;
            if (flightStatus == FlightStatus.unknown || flightStatus == FlightStatus.canceled)
            {
                dateTime = "U/N";
            }
            return dateTime;
        }

        public DateTime getDepartureDateTime()
        {
            DateTime actualDateTime = DateTime.Now;
            DateTime departureDateTime = DateTime.MinValue;
            switch ((int)getFlightStatus())
            {
                case 3:
                    departureDateTime = actualDateTime.AddMinutes(new Random().Next(120));
                    break;
                case 7:
                    departureDateTime = actualDateTime.AddMinutes(new Random().Next(240));
                    break;
                case 5:
                    departureDateTime = actualDateTime.AddMinutes(new Random().Next(240));
                    break;
            }
            return departureDateTime;
        }

        public DateTime getArrivalDateTime()
        {
            DateTime actualDateTime = DateTime.Now;
            DateTime ArrivalDateTime = DateTime.MinValue;
            switch ((int)getFlightStatus())
            {
                case 3:
                    ArrivalDateTime = actualDateTime.AddMinutes(-30);
                    break;
                case 4:
                    ArrivalDateTime = actualDateTime.AddMinutes(new Random().Next(15));
                    break;
                case 5:
                    ArrivalDateTime = actualDateTime.AddMinutes(new Random().Next(240));
                    break;
            }
            return ArrivalDateTime;
        }

        //public Flight(int id) : this()
        //{
        //    airline = getAirline();
        //    destinationCityAndPort = getCity();
        //    flightNumber =  getFlightNumber();// разобраться с nullReff
        //    gate = getGate();
        //    terminal = getTerminal();
        //    flightStatus = getFlightStatus();
        //    if (getFlightDirection() == FlightDirection.departure)
        //    {
        //        departureDateTime = getDepartureDateTime();
        //    }
        //    else if (getFlightDirection() == FlightDirection.arrival)
        //    {
        //        arriveDateTime = getArrivalDateTime();
        //    }
        //    else
        //    {
        //        flightTime = "U/N";
        //    }
        //}
    }

    public class FlightDataManager
    {
        public static Flight[] getFlight()
        {
            
            Flight flight = new Flight();
            Flight[] flightData = new Flight[10];

            for (int i = 0; i < flightData.Length; i++)
            {
                System.Threading.Thread.Sleep(500);
                //flight = new Flight(i);
                flightData[i].airline = flight.getAirline();
                flightData[i].flightNumber = flight.getFlightNumber(flightData[i].airline);
                flightData[i].destinationCityAndPort = flight.getCity();
                flightData[i].terminal = flight.getTerminal();
                flightData[i].flightStatus = flight.getFlightStatus();
                flightData[i].gate = flight.getGate();
                flightData[i].airline = flight.getAirline();
                
            }

            for (int i = 0; i < flightData.Length; i++)
            {
                Console.WriteLine(flightData[i].airline + flightData[i].destinationCityAndPort + flightData[i].flightNumber + flightData[i].terminal + flightData[i].gate + flightData[i].flightStatus + flightData[i].departureDateTime + flightData[i].arriveDateTime);
            }




            return flightData;
        }
    }
}