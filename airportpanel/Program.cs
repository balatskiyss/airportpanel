using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportPanel
{
    enum MainMenuChoice { Show = 1, Add, Edit, Delete, Search, Emergency, Exit };

    class Program
    {
        static void Main(string[] args){


            FlightDataManager.getFlight();                
                
                
            Console.WriteLine("Main menu:");
            int j = 0;
            foreach (String i in Enum.GetNames(typeof(MainMenuChoice))){
                j++;
                Console.WriteLine(j + ". " + i);
            }
            Console.WriteLine("\nChoose your action: ");

            bool flag = true;
            while (flag){
                string mainMenuUserChoice = Console.ReadLine();
                if (mainMenuUserChoice == ""){
                    Console.WriteLine("Sorry, but didn't entered any value. Please try again!");
                    mainMenuUserChoice = Console.ReadLine();
                }
                try{
                    int intMainMenuUserChoice = Int32.Parse(mainMenuUserChoice);
                    if (intMainMenuUserChoice < 0 || intMainMenuUserChoice > (int)MainMenuChoice.Exit){
                        Console.WriteLine("You've entered value what doesn't exist. Please try again!");
                    }
                    switch (intMainMenuUserChoice){
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        case 6:
                            break;
                        case 7:
                            Console.WriteLine("Goodbye! Have a nice day");
                            flag = false;
                            break;
                    }
                }
                catch (FormatException e){
                    Console.WriteLine("Sorry, entered value not a number, try again");
                }
            }
        }
    }
}

